import _ from "lodash";
import React from "react";

export default (statics, components) => {
  return statics.filter(q => _.has(components, q.component)).map((q, i) => {
    const Q = components[q.component];
    return <Q {...q} key={i} />;
  });
};
